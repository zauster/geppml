
GEPPML package
====================

This package provides some functions to do counterfactual analysis as
described in

> Yotov, Yoto V., et al. "An Advanced Guide to Trade Policy Analysis:
> The Structural Gravity Model." World Trade Organization, Geneva
> (2016).

and in 

> Anderson, James E., Mario Larch, and Yoto Yotov. "Estimating general
> equilibrium trade policy effects: GE PPML." (2015).


(Probably) the workflow will look like this:

```r

##============================================================
## 0. Step: Define the counterfactual scenario


##------------------------------------------------------------
## Function parameter

## data
## formula
## maxiter


##============================================================
## 1. Step: Do the base estimation

base_est <-
  geppml_estGravity(dots,
                    dep_var = dep_var,
                    fta_vars = c(fta_treaties,
                                 ## "cumCT_depth",
                                 "ifta",
                                 "rest"),
                    feglm_config = feglm.config,
                    center = TRUE)
## summary(base_est, type = "sandwich")
summary(base_est, type = "clustered", cluster = ~ exp + imp + year)


##============================================================
## 2. Step: prepare the Baseline model
## Scenario: Effects of NAFTA
this_year <- 1994
nafta <- c("MEX", "USA", "CAN")


conf <- list(
  exp.fe.var = "exp",
  imp.fe.var = "imp",
  sigma = 7,
  ## mu = 0.928,
  ref.code = "JPN"
)

res_estimation <- base_est
config <- conf
subset_year <- this_year
dep_var <- trade_var

cfl_yr <- prepBaselineModel(res_estimation = base_est,
                            ## res_fe = fe,
                            ## res_coefs = res.coefs,
                            config = conf,
                            subset_year = this_year,
                            dep_var = trade_var)



##============================================================
## 3. Step: calculate the counterfactual model
cfl_scenario <- copy(cfl_yr)

## Create counterfactual variables as copies of the original variables
for(this_var in names(coef(base_est))) {
  this_var_cfl <- paste0(this_var, "_cfl")
  cfl_scenario[, (this_var_cfl) := get(this_var)]
}

## Scenario: set the NAFTA treaty dummy to 0
cfl_scenario[exp %in% nafta & imp %in% nafta & exp != imp, rta_cfl := 0]


## ------------------------------------------------------------
## Calculate the counterfactual scenario
cfl_res <- calcCounterfactualModel(cfl_data = cfl_scenario,
                                   config = conf,
                                   res_estimation = base_est,
                                   dep_var = trade_var,
                                   ## tol = 0.00001,
                                   max.iter = 50)

## names(cfl_res)

cfl_agg <- cfl_res[["aggregate_results"]]

setorder(cfl_agg, change_rGDP)
cfl_agg[country %chin% nafta, ]

```
